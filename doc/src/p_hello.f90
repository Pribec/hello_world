!> 
!   Example program that calls the [[hello]] subroutine.
!
program say_hello

    use m_hello, only: hello
    implicit none

    call hello("Ivan")

end program