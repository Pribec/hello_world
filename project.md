---
project: hello_world
src_dir: ./src
output_dir: ./doc
project_github: https://gitlab.com/Pribec/hello_world
author: Ivan Pribec
author_description: Researcher at Technical University of Munich
summary: A Hello World example in Fortran.
github: https://gitlab.com/Pribec/
email: ivan.pribec@tum.de
predocmark_alt: >
predocmark: <
docmark_alt:
docmark: !
display: public
display: private
source: true
graph: true
preprocess: false
version: 0.1
---

Hi, my name is ${USER}.

This is just a useless example.