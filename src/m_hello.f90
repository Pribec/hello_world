module m_hello

    implicit none

    private

    public :: hello

contains

!>
!   Prints "Hello `name`" to the standard output.
!
    subroutine hello(name)
        character(len=*), intent(in) :: name

        print *, "Hello "//name//"!"
    end subroutine

end module
